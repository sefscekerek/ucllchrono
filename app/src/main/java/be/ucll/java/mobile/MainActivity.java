package be.ucll.java.mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class MainActivity extends AppCompatActivity {

    private TextView textCounter;
    private Button btnStarter;
    private Button btnStopper;
    long startTime;
    long duration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textCounter = findViewById(R.id.textCounter);
        btnStarter = findViewById(R.id.btnStarter);
        btnStopper = findViewById(R.id.btnStopper);
        textCounter.setText("00:00");

        btnStarter.setOnClickListener(view -> {
            String counter = textCounter.getText().toString();
            textCounter.setText(getText(R.string.textCounter_Text));
            startTime = SystemClock.elapsedRealtime();
            btnStarter.setEnabled(false);
            btnStopper.setEnabled(true);
        });

        btnStopper.setOnClickListener(view -> {
            String counter = textCounter.getText().toString();
            duration = SystemClock.elapsedRealtime() - startTime;
            SimpleDateFormat formatter = new SimpleDateFormat("mm:ss.SS");
            textCounter.setText(String.valueOf((formatter.format(duration))));
            btnStopper.setEnabled(false);
            btnStarter.setEnabled(true);
        });



    }
}